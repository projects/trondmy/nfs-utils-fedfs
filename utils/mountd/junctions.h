/*
 * utils/mountd/junctions.h
 *
 * Copyright (c) 2009 Trond Myklebust <Trond.Myklebust@netapp.com>
 */

#undef HAVE_JUNCTIONS
#if defined(HAVE_LIBATTR) && defined(HAVE_LIBFEDFS) && defined(NFS4_SUPPORTED)
#define HAVE_JUNCTIONS 1
#endif /* defined(HAVE_LIBATTR) && defined(HAVE_LIBFEDFS) && defined(NFS4_SUPPORTED) */

#ifdef HAVE_JUNCTIONS

extern struct mexport *nfsd_find_junction(char *dom, char *path);

#else

static inline struct mexport *nfsd_find_junction(const char *dom, const char *path)
{
	return NULL;
}

#endif /* HAVE_JUNCTIONS */

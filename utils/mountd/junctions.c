/*
 * utils/mountd/junctions.c
 *
 * Copyright (c) 2009 Trond Myklebust <Trond.Myklebust@netapp.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "xmalloc.h"
#include "xlog.h"
#include "nfslib.h"
#include "exportfs.h"
#include "junctions.h"

#if defined(HAVE_JUNCTIONS)

#include <xattr.h>
#include <fedfs.h>

#ifndef XATTR_TRUSTED_PREFIX
#define XATTR_TRUSTED_PREFIX "trusted."
#endif

#define XATTR_JUNCTION_PREFIX XATTR_TRUSTED_PREFIX "junction."
#define XATTR_JUNCTION_TYPE XATTR_JUNCTION_PREFIX "type"
#define XATTR_JUNCTION_NSDB XATTR_JUNCTION_PREFIX "nsdb"
#define XATTR_JUNCTION_UUID XATTR_JUNCTION_PREFIX "uuid"

static fedfs_fsl_info_t *junction_get_fileset_locations(char *uuid, char *nsdb)
{
	fedfs_nsdb_t fsnNsdbName = {
		.addr.hostname = nsdb,
		.addr.port = 0,
	};
	fedfs_fsn_t fsn = {
		.fsnNsdbName = fsnNsdbName,
		.fsnUuid = uuid,
	};
	fedfs_fsl_info_t *fsls = NULL;
	int err;

	err = fedfs_resolve_fsn(&fsn, &fsnNsdbName, &fsls);
	switch (err) {
	case FEDFS_OK:
		xlog(L_NOTICE, "mountd: Found fileset location info for nsdbName=%s uuid=%s\n",
				nsdb, uuid);
		return fsls;
	case FEDFS_ERR_LDAP_INIT:
	case FEDFS_ERR_LDAP_OPT3:
	case FEDFS_ERR_LDAP_BIND:
		xlog(L_WARNING, "mountd: LDAP connection to %s failed with fedfs error %d\n", nsdb, err);
		break;
	case FEDFS_ERR_NO_MATCH:
	case FEDFS_ERR_NO_FSN_MATCH:
	case FEDFS_ERR_NO_FSL_MATCH:
		xlog(L_WARNING, "mountd: LDAP search failed for nsdbName=%s uuid=%s\n"
				"        fedfs error = %d",
				nsdb, uuid, err);
		break;
	default:
		xlog(L_NOTICE, "mountd: fedfs lookup of uuid %s failed with error %d", uuid, err);
	}
	return NULL;
}

static void junction_free_fileset_locations(fedfs_fsl_info_t *fsls)
{
	if (fsls != NULL)
		fedfs_free_fsl_info(fsls);
}

static long junction_get_fslttl(const fedfs_fsl_t *fsl)
{
	return fsl->fslTTL;
}

static char *junction_get_fslhostname(const fedfs_fsl_t *fsl)
{
	return fsl->fslHost.hostname;
}

static char *junction_get_fslnfspath(const fedfs_fsl_t *fsl)
{
	return fsl->data.nfs.fslNfsPath;
}

static char *junction_parse_fsl_info(unsigned int fsl_num, fedfs_fsl_t *fsl, char *buf, size_t buflen)
{
	const char *fmtstring = "%s@%s";
	const char *last_path = NULL;
	char *ret = buf;
	ssize_t len;
	unsigned int i;

	if (fsl_num == 0)
		return NULL;
	len = snprintf(buf, buflen, "refer=");
	if (len < 0 || len == buflen)
		return NULL;
	buflen -= len;
	buf += len;

	for (i = 0; i < fsl_num; i++,fsl++) {
		char *path = junction_get_fslnfspath(fsl);
		char *hostname = junction_get_fslhostname(fsl);

		if (last_path && strcmp(path, last_path) == 0) {
			len = snprintf(buf, buflen,"+%s", hostname);
			goto check_buflen;
		}

		len = snprintf(buf, buflen, fmtstring, path, hostname);
		fmtstring = ":%s@%s";
		last_path = path;
check_buflen:
		if (len < 0 || len == buflen)
			return NULL;
		buflen -= len;
		buf += len;
	}
	return ret;
}

static int junction_get_ttl(unsigned int fsl_num, fedfs_fsl_t *fsl)
{
	unsigned long ttl;
	unsigned int i;

	ttl = INT_MAX;

	for (i = 0; i < fsl_num; i++) {
		unsigned long newttl;

		newttl = junction_get_fslttl(&fsl[i]);
		if (newttl < ttl)
			ttl = newttl;
	}
	return (int)ttl;
}

static struct exportent *nfsd_fake_exportent(char *path, char *uuid, char *nsdbname)
{
	char buf[8192];
	char *options;
	struct exportent *ret = NULL;
	fedfs_fsl_info_t *fsls;

	fsls = junction_get_fileset_locations(uuid, nsdbname);
	if (!fsls)
		goto out;
	options = junction_parse_fsl_info(fsls->fsl_num, fsls->fsl_array,
			buf, sizeof(buf));
	if (!options)
		goto out_free_fsls;
	ret = mkexportent("*", path, options);
	if (ret) {
		ret->e_ttl = junction_get_ttl(fsls->fsl_num, fsls->fsl_array);
		ret->e_uuid = strdup(uuid);
	}
out_free_fsls:
	junction_free_fileset_locations(fsls);
out:
	return ret;
}

static struct mexport *nfsd_fake_export(char *path, char *uuid, char *nsdbname)
{
	struct mexport *ret = NULL;
	struct exportent *xep;

	xep = nfsd_fake_exportent(path, uuid, nsdbname);
	if (xep != NULL)
		ret = export_create(xep, 0);
	return ret;
}

static char *fgetxattr_string(int fd, const char *name)
{
	char attrstr[256];
	ssize_t len;

	len = fgetxattr(fd, name, attrstr, sizeof(attrstr) - 1);
	if (len <= 0)
		return NULL;
	attrstr[len] = '\0';
	return strndup(attrstr, sizeof(attrstr));
}

static char *junction_get_nsdbname(int fd)
{
	return fgetxattr_string(fd, XATTR_JUNCTION_NSDB);
}

static char *junction_get_uuid(int fd)
{
	return fgetxattr_string(fd, XATTR_JUNCTION_UUID);
}

static int object_is_junction(int fd)
{
	struct stat statbuf;
	char *s = NULL;
	int ret = 0;

	if (fstat(fd, &statbuf))
		goto out;
	if (!S_ISDIR(statbuf.st_mode))
		goto out;

	s = fgetxattr_string(fd, XATTR_JUNCTION_TYPE);
	if (!s || strcmp(s, "fedfs") != 0)
		goto out;
	ret = 1;
out:
	free(s);
	return ret;
}

struct mexport *nfsd_find_junction(char *dom, char *path)
{
	struct mexport *exp = NULL;
	char *uuid = NULL;
	char *nsdbname = NULL;
	int fd;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		goto out;
	if (!object_is_junction(fd))
		goto out_close;
	xlog(L_NOTICE, "mountd: found junction at %s!", path);
	uuid = junction_get_uuid(fd);
	if (!uuid)
		goto out_close;
	xlog(L_NOTICE, "Found uuid = %s!\n", uuid);
	nsdbname = junction_get_nsdbname(fd);
	if (!nsdbname)
		goto out_close;
	xlog(L_NOTICE, "Found nsdbName = %s!\n", nsdbname);
	exp = nfsd_fake_export(path, uuid, nsdbname);
out_close:
	free(nsdbname);
	free(uuid);
	close(fd);
out:
	return exp;
}

#endif /* HAVE_JUNCTIONS */

dnl Checks for extended attribute library and headers
dnl
AC_DEFUN([AC_LIBFEDFS], [

  AC_ARG_WITH([fedfsinclude],
              [AC_HELP_STRING([--with-fedfsinclude=DIR],
                              [use libfedfs headers in DIR])],
              [fedfs_header_dir=$withval],
              [fedfs_header_dir=/usr/include/fedfs])

  AC_ARG_WITH([libfedfs],
              [AC_HELP_STRING([--with-libfedfs=DIR],
                              [use libfedfs library in DIR])],
              [libfedfs_dir="-L${withval}"],
              [libfedfs_dir=""])

  dnl if --enable-libfedfs was specifed, the following components
  dnl must be present, and we set up HAVE_ macros for them.

  if test "$enable_libfedfs" != "no"; then
    saved_LDFLAGS="${LDFLAGS}"
    LDFLAGS="${libfedfs_dir} ${saved_LDFLAGS}"

    dnl look for the library and define LIBFEDFS if found
    AC_CHECK_LIB([fedfs], [fedfs_resolve_fsn],
		 [LIBFEDFS="${libfedfs_dir} -lfedfs"],
		 [if test "$enable_libfedfs" = "yes"; then
			AC_MSG_ERROR([libfedfs not found.])
		  else
			AC_MSG_WARN([libfedfs not found. LIBFEDFS disabled!])
			enable_libfedfs="no"
		  fi])

    LDFLAGS="${saved_LDFLAGS}"
  fi

  if test "$enable_libfedfs" != "no"; then
    dnl also must have the headers installed where we expect
    dnl look for headers; add -I compiler option if found
    AC_CHECK_HEADERS([${fedfs_header_dir}/fedfs.h],
		      [LIBFEDFS_INCLUDE="-I${fedfs_header_dir}"
			AC_DEFINE([HAVE_LIBFEDFS], 1,
			[Define to 1 if you have the `fedfs' library (-lfedfs).])
		      ],
		      [if test "$enable_libfedfs" = "yes"; then
			 AC_MSG_ERROR([libfedfs headers not found.])
		       else
			 AC_MSG_WARN([libfedfs headers not found. LIBFEDFS disabled!])
			 enable_libfedfs="no"
		       fi])
  fi

  AC_SUBST(LIBFEDFS)
  AC_SUBST(LIBFEDFS_INCLUDE)

])dnl

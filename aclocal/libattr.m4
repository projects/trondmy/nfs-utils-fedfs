dnl Checks for extended attribute library and headers
dnl
AC_DEFUN([AC_LIBATTR], [

  AC_ARG_WITH([attrinclude],
              [AC_HELP_STRING([--with-attrinclude=DIR],
                              [use libattr headers in DIR])],
              [attr_header_dir=$withval],
              [attr_header_dir=/usr/include/attr])

  AC_ARG_WITH([libattr],
              [AC_HELP_STRING([--with-libattr=DIR],
                              [use libattr library in DIR])],
              [libattr_dir="-L${withval}"],
              [libattr_dir=""])

  dnl if --enable-libattr was specifed, the following components
  dnl must be present, and we set up HAVE_ macros for them.

  if test "$enable_libattr" != "no"; then
    saved_LDFLAGS="${LDFLAGS}"
    LDFLAGS="${libattr_dir} ${saved_LDFLAGS}"

    dnl look for the library and define LIBATTR if found
    AC_CHECK_LIB([attr], [fgetxattr],
		 [LIBATTR="${libattr_dir} -lattr"],
		 [if test "$enable_libattr" = "yes"; then
			AC_MSG_ERROR([libattr not found.])
		  else
			AC_MSG_WARN([libattr not found. LIBATTR disabled!])
			enable_libattr="no"
		  fi])
    LDFLAGS="${saved_LDFLAGS}"
  fi

  if test "$enable_libattr" != "no"; then
    dnl also must have the headers installed where we expect
    dnl look for headers; add -I compiler option if found
    AC_CHECK_HEADERS([${attr_header_dir}/xattr.h],
		      [LIBATTR_INCLUDE="-I${attr_header_dir}"
			AC_DEFINE([HAVE_LIBATTR], 1,
			[Define to 1 if you have the `attr' library (-lattr).])
		      ],
		      [if test "$enable_libattr" = "yes"; then
			 AC_MSG_ERROR([libattr headers not found.])
		       else
			 AC_MSG_WARN([libattr headers not found. LIBATTR disabled!])
			 enable_libattr="no"
		       fi])
  fi

  AC_SUBST(LIBATTR)
  AC_SUBST(LIBATTR_INCLUDE)

])dnl
